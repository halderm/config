default: help
.PHONY: mac_bootstrap mac

help:
	@echo "make mac_bootstrap - bootstrap ansible on mac"
	@echo "make mac - call mac playbook"

mac_bootstrap:
	brew update
	brew install ansible 
	brew install python
	pip install --user ansible-lint
	pip install --user passlib
	pip install --user pycurl

mac:
	cd ansible && ansible-playbook -i inventory/localhost mac.yml
